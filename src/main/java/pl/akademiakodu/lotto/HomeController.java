package pl.akademiakodu.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
//@controller i klasa ktora storzylismy bedzie odpowiedzialna za zadania http

@Controller
public class HomeController {
    //metoda do obslugiwania zadania
    //ResponsBody , ze rezultat zwrocony bedzie Stringiem

    @ResponseBody
    @GetMapping("/") // po tej sciezce wywolana jest metoda hello

    public String Hello() {
        return "Witaj świecie" ;
    }

    @ResponseBody

    @GetMapping("/bye") // po tej sciezce wywolana jest metoda hello
    public String Byebye() {
        return "Do widzenia";
    }

    @GetMapping("/welcome")
    public String welcome() {
        return "witam" ; //nie ma response body wiec nie zwraca stringa tylko html,
        // ktory bedzie w resources/temlates/witam(z returna bierze nazwe).html

    }
    @GetMapping("/product")
    public String produkt() {
        return "product";
    }
    @GetMapping("/lotto")
    public String generateLotto(ModelMap map){
        LottoGenerator lottoGenerator = new LottoGenerator() ;
        map.put("numbers",lottoGenerator.generate()) ;
        return "lotto" ;
    }
    }


