package pl.akademiakodu.lotto;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LottoGenerator {

    public Set<Integer> generate() {
        Set<Integer> setLotto = new HashSet<>();
        Random random = new Random();
        while (setLotto.size() != 6) {
        setLotto.add(random.nextInt(49)+1) ;
        }

        return setLotto;
    }
}
